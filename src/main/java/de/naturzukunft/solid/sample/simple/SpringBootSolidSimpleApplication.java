package de.naturzukunft.solid.sample.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSolidSimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSolidSimpleApplication.class, args);
	}

}
